pipeline {
    agent any
    tools {
        nodejs "node"
    }
    stages {
        stage('Increment Version') {
            steps {
                script {
                    // Enter the "app" directory, where package.json is located
                    dir("app") {
                        // Update the application version in the package.json file with one of these release types: patch, minor, or major
                        // This will commit the version update
                        sh "npm version minor"

                        // Read the updated version from the package.json file
                        def packageJson = readJSON file: 'package.json'
                        def version = packageJson.version

                        // Set the new version as part of IMAGE_NAME
                        env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                    }

                    // Alternative solution without Pipeline Utility Steps plugin: 
                    // def version = sh (returnStdout: true, script: "grep 'version' package.json | cut -d '\"' -f4 | tr '\\n' '\\0'")
                    // env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Run Tests') {
            steps {
                script {
                    // Enter the "app" directory, where package.json and tests are located
                    dir("app") {
                        // Install all dependencies needed for running tests
                        sh "npm install"
                        sh "npm run test"
                    } 
                }
            }
        }
        stage('Build and Push Docker Image') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker-hub-repo', usernameVariable: 'USER', passwordVariable: 'PASS')]){
                    sh "docker build -t famzyactivity/my-repo:${IMAGE_NAME} ."
                    sh 'echo $PASS | docker login -u $USER --password-stdin'
                    sh "docker push famzyactivity/my-repo:${IMAGE_NAME}"
                }
            }
        }
        stage('Commit Version Update') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credential', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
                        // Configure Git for the first-time run
                        sh 'git config --global user.email "jenkins@gmail.com"'
                        sh 'git config --global user.name "jenkins"'
                        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/afamchidolue/jenkins-nodejs.git'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:refs/heads/main'
                    }
                }
            }
        }
    }
}
